﻿using System;
namespace ImageToolsCSharp.Tools.Common
{
    public interface IImageTool
    {
        IImage ApplyTool(IImage target);

        void AddParameter(Common.ParameterName name, Common.IParameter<Double> value);

        Common.IParameter<Double> GetParameter(Common.ParameterName name);

        void RemoveParameter(Common.ParameterName name);

        Common.Tool ThisTool { get; }
    }
}
