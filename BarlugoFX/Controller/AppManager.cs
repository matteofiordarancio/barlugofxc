using System;
using System.Drawing.Imaging;
using ImageToolsCSharp;
using ImageToolsCSharp.Tools;
using ImageToolsCSharp.Tools.Common;

namespace BarlugoFX.Controller
{
    ///<summary>
    ///The main controller class.
    ///Now the only usable tool is Brightness. All the other ones are not working.
    ///</summary>
    public class AppManager : IAppManager
    {
        //multipliers used to adapt the input from the view to the model
        private static readonly float HSB_MULTIPLIER = 0.01f;
        private static readonly float WB_MULTIPLIER = 0.015f;
        private static readonly float VIBRANCE_MULTIPLIER = 0.01f;
        private static readonly float BW_MULTIPLIER = 0.004f;
        private static readonly float BW_SHIFTER = 0.8f;
        private IImage image;
        private readonly IImageTool exposure;
        private readonly IImageTool contrast;
        private readonly IImageTool brightness;
        private readonly IImageTool wb;
        private readonly IImageTool saturation;
        private readonly IImageTool hue;
        private readonly IImageTool srgb;
        private readonly IImageTool bw;
        private readonly IImageTool vibrance;
        private readonly IOManager fileManager;
        
        public IImage Image { get; set; }

        public double Exposure 
        {
            set
            {
                exposure.AddParameter(/*ParametersName.EXPOSURE*/ParameterName.BRIGHTNESS, new Parameter<double>(value));
                image = exposure.ApplyTool(image);
                exposure.RemoveParameter(ParameterName.BRIGHTNESS);
            }
        }
        public double Contrast 
        {
            set
            {
                contrast.AddParameter(/*ParametersName.CONTRAST*/ParameterName.BRIGHTNESS, new Parameter<double>(value));
                image = contrast.ApplyTool(image);
                contrast.RemoveParameter(ParameterName.BRIGHTNESS);
            }
        }
        public double Brightness 
        {
            set
            {
                brightness.AddParameter(ParameterName.BRIGHTNESS, new Parameter<double>(value));
                image = brightness.ApplyTool(image);
                brightness.RemoveParameter(ParameterName.BRIGHTNESS);
            }
        }
        public double WhiteBalance 
        {
            set
            {
                wb.AddParameter(/*ParametersName.WHITEBALANCE*/ParameterName.BRIGHTNESS, new Parameter<double>(value));
                image = wb.ApplyTool(image);
                wb.RemoveParameter(ParameterName.BRIGHTNESS);
            }
        }
        public double Saturation
        {
            set
            {
                saturation.AddParameter( /*ParametersName.SATURATION*/ParameterName.BRIGHTNESS, new Parameter<double>(value));
                image = saturation.ApplyTool(image);
                saturation.RemoveParameter(ParameterName.BRIGHTNESS);
            }
        }
        public double Hue 
        {
            set
            {
                hue.AddParameter(/*ParametersName.HUE*/ParameterName.BRIGHTNESS, new Parameter<double>(value));
                image = hue.ApplyTool(image);
                hue.RemoveParameter(ParameterName.BRIGHTNESS);
            }
        }
        public double BlackAndWhite 
        {
            set
            {
                bw.AddParameter(/*ParametersName.BLACKANDWHITE*/ParameterName.BRIGHTNESS, new Parameter<double>(value));
                image = bw.ApplyTool(image);
                bw.RemoveParameter(ParameterName.BRIGHTNESS);
            }
        }
        public double Vibrance 
        {
            set
            {
                vibrance.AddParameter(/*ParametersName.VIBRANCE*/ParameterName.BRIGHTNESS, new Parameter<double>(value));
                image = vibrance.ApplyTool(image);
                vibrance.RemoveParameter(ParameterName.BRIGHTNESS);
            }
        }
        /// <summary>
        /// The class constructor
        /// </summary>
        /// <param name="file">the file path</param>
        /// <exception cref="ArgumentNullException">if the param is null</exception>
        public AppManager(Uri file)
        {   
            //init. TEMP. only brightness is working.
            exposure = new BrightNess();
            contrast = new BrightNess();
            brightness = new BrightNess();
            wb = new BrightNess();
            saturation = new BrightNess();
            hue = new BrightNess();
            srgb = new BrightNess();
            bw = new BrightNess();
            vibrance = new BrightNess();
            fileManager = new IOManager();
            if (file == null) throw new ArgumentNullException(nameof(file));
            fileManager.LoadImageFromFile(file);
        }

        public void LoadNewImage(Uri file)
        {
            if (file == null) throw new ArgumentNullException(nameof(file));
            fileManager.LoadImageFromFile(file);
        }

        public void ExportImage(Uri path, ImageFormat format)
        {
            fileManager.ExportImage(image, path, format);
        }

        public void ExportImage(Uri path, int quality)
        {
            fileManager.ExportJPEGWithQuality(image, path, quality);
        }
    }
}