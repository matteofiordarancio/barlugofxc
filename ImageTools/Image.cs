﻿using System;
using System.Drawing;
namespace ImageToolsCSharp.ImageTools
{
    public class Image : IImage
    {
        private readonly int width;
        private readonly int height;
        private const Boolean hasAlphaChannel = true; //Bitmap type always have alpha channel
        private readonly int[,] pixels;

        public Image(Bitmap target)
        {
            this.width = target.Width;
            this.height = target.Height;
            pixels = new int[height, width];
            for (int i = 0; i < target.Height; i++)
            {
                for (int j = 0; j < target.Width; j++)
                {
                    pixels[i, j] = target.GetPixel(j, i).ToArgb();
                }
            }
        }

        public Image(int[,] toCopy){
            height = toCopy.GetLength(0);
            width = toCopy.GetLength(1);
            pixels = toCopy;
        }

        public int Width => this.width;
        public int Height => this.height;
        public int[,] ImageRGB => this.pixels;
    }
}
