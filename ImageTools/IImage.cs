﻿using System;
namespace ImageToolsCSharp
{
    public interface IImage
    {
        int[,] ImageRGB { get; }
        int Width { get; }
        int Height { get; }
    }
}
