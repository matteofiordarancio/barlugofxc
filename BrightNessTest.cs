﻿using System;
using NUnit.Core;
using NUnit.Framework;
using System.Drawing;

namespace ImageToolsCSharp
{   
    public class BrightNessTest
    {
        readonly IImage target = new ImageTools.Image(new Bitmap(@"../../JPEGS/begin.JPG"));
        readonly IImage expectedOuput = new ImageTools.Image(new Bitmap(@"../../JPEGS/expected.JPG"));

        [Test]
        public void TestInvalidParameter(){
            Tools.Common.IImageTool bright = new Tools.BrightNess();
            try{
                bright.AddParameter(Tools.Common.ParameterName.NOTVALID, new Tools.Common.Parameter<Double>(150));
                Assert.Fail("The Parameter Name is not valide");
            }catch(Exception){
                Assert.IsTrue(true);
            }
        }

        [Test]
        public void TestRepeatedParameter()
        {
            Tools.Common.IImageTool bright = new Tools.BrightNess();
            bright.AddParameter(Tools.Common.ParameterName.BRIGHTNESS, new Tools.Common.Parameter<Double>(150));
            try
            {
                bright.AddParameter(Tools.Common.ParameterName.BRIGHTNESS, new Tools.Common.Parameter<Double>(150));
                Assert.Fail("You need to remove the parameter first");
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
        }

        [Test]
        public void TestDoubleParameter()
        {
            Tools.Common.IImageTool bright = new Tools.BrightNess();
            bright.AddParameter(Tools.Common.ParameterName.BRIGHTNESS, new Tools.Common.Parameter<Double>(150.5));
            try
            {
                bright.ApplyTool(target);
                Assert.Fail("Should be an int");
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
        }

        [Test]
        public void TestOutOfRangeParameter()
        {
            Tools.Common.IImageTool bright = new Tools.BrightNess();
            bright.AddParameter(Tools.Common.ParameterName.BRIGHTNESS, new Tools.Common.Parameter<Double>(256));
            try
            {
                bright.ApplyTool(target);
                Assert.Fail("Should be between -255 and 255");
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
        }

        [Test]
        public void ResultAsExpected()
        {
            Tools.Common.IImageTool bright = new Tools.BrightNess();
            bright.AddParameter(Tools.Common.ParameterName.BRIGHTNESS, new Tools.Common.Parameter<Double>(150));

            IImage result = bright.ApplyTool(target);
            for (int i = 0; i < target.Height; i++)
            {
                for (int j = 0; j < target.Width; j++)
                {
                    if (result.ImageRGB[i, j] != expectedOuput.ImageRGB[i, j])
                    {
                        Assert.Fail("Result shall be the same as expectedOutput");    
                    }
                }    
            }
        }
    }
}
